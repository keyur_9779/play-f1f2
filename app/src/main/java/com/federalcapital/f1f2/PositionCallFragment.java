package com.federalcapital.f1f2;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.adapter.MyAdapter;
import com.federalcapital.f1f2.model.F1F2Datas;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PositionCallFragment extends Fragment {


    private static final String TAG = "tedt";
    RecyclerView recyclerView;
    ProgressBar dotProgressBar;
    ArrayList<F1F2Datas> ac = new ArrayList<>();
    private MyAdapter mAdapter;

    Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_positioncall_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        dotProgressBar = view.findViewById(R.id.progressBar_cyclic);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new MyAdapter(getActivity());
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        layout.setSmoothScrollbarEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(mAdapter);

        readPostion();

    }

    int counter;


    public void searchFileter(String query) {
        mAdapter.filter(query);

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            readPostion();

        }
    };

    public void readPostion() {
        if (counter < 1) {
            dotProgressBar.setVisibility(View.VISIBLE);
        }
        if (haveNetworkConnection()) {
            Call<ResponseBody> call = RetroConfig.retrofit().readPosition("Bearer " + AppPrefConfig.getPref().getToken(), "application/x-www-form-urlencoded"
                    , "application/json");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        try {


                            counter = 2;

                            String responseString = response.body().string().trim();
                            Log.e(TAG, "response fragment" + responseString);
                            try {
                                JSONObject res = new JSONObject(responseString);

                                /*if (res.has("virtualbalance")) {
                                    //virtualbalance

                                    ((MainActivity) getActivity()).tabLayout.getTabAt(0).setText("Virtual Money : INR " + res.getJSONObject("virtualbalance")
                                            .getString("virtualbalance"));


                                }*/


                                String rr = res.getString("data");
                                JSONArray jsonArray = new JSONArray(rr);

                                String rrs = res.getString("data_scr");
                                JSONArray js = new JSONArray(rrs);

                                JSONObject jo = js.getJSONObject(0);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject mJsonObject = jsonArray.getJSONObject(i);
                                    Iterator<String> iter = mJsonObject.keys();
                                    while (iter.hasNext()) {
                                        String key = iter.next();

                                        try {
                                            String ress = mJsonObject.get(key).toString();
                                            String re_scr = "";
                                            re_scr = jo.get(key).toString();
                                            JSONObject js_ress = new JSONObject(ress);
                                            JSONObject js_scr = new JSONObject(re_scr);
                                            String signal = js_ress.getString("signal");
                                            String scrip = js_scr.getString("stock") + "/-";
                                            String stllow = js_ress.getString("stllow") + "/-";
                                            String stlhigh = js_ress.getString("stlhigh") + "/-";
                                            String rate = js_scr.getString("rate") + "%";

                                            String ex = "", targetHit = "";
                                            try {
                                                ex = js_ress.getString("exit_stl") + "/-";
                                            } catch (JSONException e) {

                                            }
                                            try {
                                                targetHit = js_ress.getString("exit_trg") + "/-";
                                            } catch (JSONException e) {

                                            }
                                            ac.add(new F1F2Datas(key, scrip, signal, stllow, stlhigh, ex, rate, targetHit));
                                        } catch (JSONException e) {
                                            // e.printStackTrace();
                                        }
                                    }
                                }
                                if (dotProgressBar.getVisibility() == View.VISIBLE) {
                                    dotProgressBar.setVisibility(View.GONE);
                                }
                                mAdapter.updateAll(ac);
                                recyclerView.setAdapter(mAdapter);
                                handler.postDelayed(runnable, 60000);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                handler.postDelayed(runnable, 10000);

                                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong...please try later", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.e(TAG, "error");
                }
            });
        } else {
            showNetworkError();
        }

    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    void showNetworkError() {
        Dialog dialog = new Dialog(getActivity(), R.style.AppTheme);
        dialog.setContentView(R.layout.internetcheck_custom_dialog);
        dialog.show();
        Button btnRetry = dialog.findViewById(R.id.btn_Retry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                readPostion();
            }
        });
    }
}