package com.federalcapital.f1f2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PortfolioDataModel {

    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("buyqty")
    @Expose
    private String buyqty;
    @SerializedName("buyavg")
    @Expose
    private String buyavg;
    @SerializedName("totbuyvalue")
    @Expose
    private String totbuyvalue;
    @SerializedName("sellqty")
    @Expose
    private String sellqty;
    @SerializedName("sellavg")
    @Expose
    private String sellavg;
    @SerializedName("totalsoldvalue")
    @Expose
    private String totalsoldvalue;
    @SerializedName("profitloss")
    @Expose
    private String profitloss;
    @SerializedName("cmp")
    @Expose
    private String cmp;
    @SerializedName("sharebalance")
    @Expose
    private String sharebalance;
    @SerializedName("total")
    @Expose
    private String total;

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getBuyqty() {
        return buyqty;
    }

    public void setBuyqty(String buyqty) {
        this.buyqty = buyqty;
    }

    public String getBuyavg() {
        return buyavg;
    }

    public void setBuyavg(String buyavg) {
        this.buyavg = buyavg;
    }

    public String getTotbuyvalue() {
        return totbuyvalue;
    }

    public void setTotbuyvalue(String totbuyvalue) {
        this.totbuyvalue = totbuyvalue;
    }

    public String getSellqty() {
        return sellqty;
    }

    public void setSellqty(String sellqty) {
        this.sellqty = sellqty;
    }

    public String getSellavg() {
        return sellavg;
    }

    public void setSellavg(String sellavg) {
        this.sellavg = sellavg;
    }

    public String getTotalsoldvalue() {
        return totalsoldvalue;
    }

    public void setTotalsoldvalue(String totalsoldvalue) {
        this.totalsoldvalue = totalsoldvalue;
    }

    public String getProfitloss() {
        return profitloss;
    }

    public void setProfitloss(String profitloss) {
        this.profitloss = profitloss;
    }

    public String getCmp() {
        return cmp;
    }

    public void setCmp(String cmp) {
        this.cmp = cmp;
    }

    public String getSharebalance() {
        return sharebalance;
    }

    public void setSharebalance(String sharebalance) {
        this.sharebalance = sharebalance;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
