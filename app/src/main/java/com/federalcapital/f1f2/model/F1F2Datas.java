package com.federalcapital.f1f2.model;

/**
 * Created by deeptigarg on 04/03/16.
 */
public class F1F2Datas implements Cloneable {
    String CompanyName;
    String StockPrice;
    String Signal;
    String stoploss;
    String Target1;
    String Exit;
    String Rate;
    int type;
    String exitTar;

    public F1F2Datas(String CompanyName, String StockPrice, String Signal, String stoploss, String Target1, String Exit, String Rate, String exitTar) {
        super();
        this.CompanyName = CompanyName;
        this.StockPrice = StockPrice;
        this.Signal = Signal;
        this.stoploss = stoploss;
        this.Target1 = Target1;
        this.Exit = Exit;
        this.Rate = Rate;
        this.exitTar = exitTar;


    }

    public F1F2Datas() {

    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        this.CompanyName = companyName;
    }

    public String getSignal() {
        return Signal;
    }

    public void setSignal(String signal) {
        this.Signal = signal;
    }

    public String getStockPrice() {
        return StockPrice;
    }

    public void setStockPrice(String stockPrice) {
        this.StockPrice = stockPrice;
    }

    public String getStoploss() {
        return stoploss;
    }

    public void setStoploss(String stoploss) {
        this.stoploss = stoploss;
    }

    public String getTarget1() {
        return Target1;
    }

    public void setTarget1(String target1) {
        this.Target1 = target1;
    }

    public String getExit() {
        return Exit;
    }

    public void setExit(String exit) {
        this.Exit = exit;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        this.Rate = rate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getExitTar() {
        return exitTar;
    }

    public void setExitTar(String exitTar) {
        this.exitTar = exitTar;
    }
}
