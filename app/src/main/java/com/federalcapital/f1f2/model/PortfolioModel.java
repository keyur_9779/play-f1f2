package com.federalcapital.f1f2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PortfolioModel {

    @SerializedName("data")
    @Expose
    private List<PortfolioDataModel> data = null;

    public List<PortfolioDataModel> getData() {
        return data;
    }

    public void setData(List<PortfolioDataModel> data) {
        this.data = data;
    }

}
