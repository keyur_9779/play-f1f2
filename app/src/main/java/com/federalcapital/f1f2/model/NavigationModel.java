package com.federalcapital.f1f2.model;

public class NavigationModel {

    private int resourceID;

    private String name;

    public NavigationModel(int resourceID, String name) {
        this.resourceID = resourceID;
        this.name = name;
    }

    public int getResourceID() {
        return resourceID;
    }

    public String getName() {
        return name;
    }
}
