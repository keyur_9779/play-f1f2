package com.federalcapital.f1f2.retrofit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class DataList implements Serializable {


    @SerializedName("data")
    @Expose
    private ArrayList<DataListItems> data = null;


    public ArrayList<DataListItems> getData() {
        return data;
    }


}




