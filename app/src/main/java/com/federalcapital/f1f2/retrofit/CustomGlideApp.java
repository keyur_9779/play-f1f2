package com.federalcapital.f1f2.retrofit;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public class CustomGlideApp extends AppGlideModule {

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {

        /*if (Build.VERSION.SDK_INT < Constant.twenty) {

            OkHttpUrlLoader.Factory factory = new OkHttpUrlLoader.Factory(AppModule.getAppModule().getAPIClient().getClient());

            glide.getRegistry().replace(GlideUrl.class, InputStream.class, factory);
        } else {*/

        super.registerComponents(context, glide, registry);

        //}

    }
}
