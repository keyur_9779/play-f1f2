package com.federalcapital.f1f2.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Clusterbean Technologies Pvt. Ltd. on 14/5/18.
 */
public abstract class DataListItems {


    @Expose
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}