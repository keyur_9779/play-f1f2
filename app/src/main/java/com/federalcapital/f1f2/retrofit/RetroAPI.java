package com.federalcapital.f1f2.retrofit;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Darshita on 21-06-2019.
 */

public interface RetroAPI {


    @FormUrlEncoded
    @POST("api/clogin")
    Call<ResponseBody> insertLoginDetails(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api/register")
    Call<ResponseBody> register(
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String confirmPassword,
            @Field("name") String name,
            @Field("mobile") String mobile,
            @Field("terms") String terms,
            @Field("organization") String organization,
            @Field("stream") String stream,
            @Field("place") String place,
            @Field("referral") String referral
    );

   /* @FormUrlEncoded
    @POST("api/playf1f2posi/mob")
    @Headers({
            "Authorization: Bearer "+ AppPrefConfig.getPref().getToken(),
            "Content-Type:application/x-www-form-urlencoded"
    })
    Call<ResponseBody> readPosition(z

    );*/

    /*@FormUrlEncoded*/
    @GET("api/clientp/api")
    Call<ResponseBody> readPosition(@Header("Authorization") String header,
                                    @Header("Content-Type") String cType,
                                    @Header("Accept") String accept

    );

    @GET("api/crdval/api")
    Call<ResponseBody> getMoney(@Header("Authorization") String header,
                                @Header("Content-Type") String cType,
                                @Header("Accept") String accept

    );


    @GET("api/portfolio/api")
    Call<ResponseBody> portfolio(@Header("Authorization") String header,
                                 @Header("Content-Type") String cType,
                                 @Header("Accept") String accept

    );

    @FormUrlEncoded
    @POST("api/intra/api")
    Call<ResponseBody> readIntraday(
            @Field("_token") String token


    );

    @FormUrlEncoded
    @POST("api/password/create")
    Call<ResponseBody> forgotPassword(
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("api/transact")
    Call<ResponseBody> purchaseShare(@Header("Authorization") String header,
                                     @Field("comp") String comp,
                                     @Field("price") String price,
                                     @Field("qty") String qty,
                                     @Field("tr") String tr
    );

    @FormUrlEncoded
    @POST("api/google")
    Call<ResponseBody> googleSignIn(
            @Field("email") String email);

    @GET("api/offer")
    Call<ResponseBody> getoffers();
}
