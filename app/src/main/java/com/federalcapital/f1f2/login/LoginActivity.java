package com.federalcapital.f1f2.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;

import com.federalcapital.f1f2.BaseActivity;
import com.federalcapital.f1f2.MainActivity;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.federalcapital.f1f2.utils.Constants;
import com.federalcapital.f1f2.utils.PasswordEditText;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.refactor.lib.colordialog.PromptDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    //SHA1: 14:4A:5E:5C:A3:08:A8:BD:80:79:D8:03:C4:52:53:26:24:04:91:67
    private static final String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    PasswordEditText inputPassword;
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.link_signup)
    TextView linkSignup;
    @BindView(R.id.link_forgotpass)
    TextView linkForgotpass;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    private int RequestCode = 1208;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }


    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestCode) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            isEmailExist(account.getEmail(), account.getDisplayName());


        } catch (Exception e) {

            Log.d(TAG, " Signup exceptiom : " + e.getMessage());
            Log.d(TAG, " Signup exceptiom : " + e.getCause());
            Log.d(TAG, " Signup exceptiom : " + e.getLocalizedMessage());

        }
    }

    private void isEmailExist(String email, String displayName) {


        LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this).autoDismiss(false)
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
                .asLoading()
                .show();

        Call<ResponseBody> call = RetroConfig.retrofit().googleSignIn(email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    loadingPopup.dismiss();

                    boolean isSuccess = false;

                    if (response.isSuccessful()) {
                        String responseString = response.body().string().trim();

                        JSONObject jsonObject = new JSONObject(responseString);
                        if (jsonObject.getBoolean("success")) {
                            isSuccess = true;
                            inputPassword.setText(Constants.EMPTY);
                            inputEmail.setText(email);
                            Toast.makeText(LoginActivity.this, "Enter password to login into app!!", Toast.LENGTH_LONG).show();

                        }
                    }
                    if (!isSuccess) {
                        Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                        intent.putExtra(Constants.email, email);
                        intent.putExtra(Constants.name, displayName);
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Please try again with valid input!", Toast.LENGTH_LONG).show();


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loadingPopup.dismiss();
                Toast.makeText(LoginActivity.this, "Please try again with valid input!", Toast.LENGTH_LONG).show();

                Log.e(TAG, "error");
            }
        });
    }


    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        if (!email.isEmpty()) {

            if (!password.isEmpty()) {
                if (haveNetworkConnection()) {
                    insertLogin(email, password);
                } else {
                    showNetworkError();
                }

            } else {
                Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(LoginActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
        }


    }

    @OnClick({R.id.link_signup, R.id.google_login})
    public void onSignupViewClicked(View v) {


        switch (v.getId()) {
            case R.id.link_signup:
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);

                break;
            case R.id.google_login:


                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RequestCode);
                break;
        }
    }

    public void insertLogin(String email, String password) {


        hideKeyboard();
        LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this).autoDismiss(false)
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
                .asLoading()
                .show();

        Call<ResponseBody> call = RetroConfig.retrofit().insertLoginDetails(email, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    loadingPopup.dismiss();

                    if (response.isSuccessful()) {
                        String responseString = response.body().string().trim();


                        validateResponse(responseString);
                        AppPrefConfig.getPref().setEmail(email);


                    } else {

                        Toast.makeText(LoginActivity.this, "Email or password is not correct.", Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Email or password is not correct", Toast.LENGTH_LONG).show();


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loadingPopup.dismiss();
                Toast.makeText(LoginActivity.this, "Email or password is not correct", Toast.LENGTH_LONG).show();

                Log.e(TAG, "error");
            }
        });


    }


    private void validateResponse(String responseString) throws JSONException {


        JSONObject jsonObject = new JSONObject(responseString);

        JSONObject jsonObjectInner = jsonObject.getJSONObject("success");

        AppPrefConfig.getPref().setToken(jsonObjectInner.getString("token"));
        AppPrefConfig.getPref().setEmail(inputEmail.getText().toString());

        boolean isPlay = false;

        if (jsonObject.has("identification")) {
            JSONObject jsonObjectActive = jsonObject.getJSONObject("identification");

            if (jsonObjectActive.getString("identification").equals(Constants.one)) {
                isPlay = true;
            }
        }


        if (jsonObject.has("name")) {
            AppPrefConfig.getPref().setUserName(jsonObject.getJSONObject("name").getString("name"));
        }


        if (isPlay) {
            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_LONG).show();


            Intent start = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(start);
            finish();

        } else {

            new PromptDialog(this)
                    .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                    .setAnimationEnable(true)
                    .setTitleText("Successful")
                    .setContentText("Congratulations!, Your account will be activated in 4 hours")
                    .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                        @Override
                        public void onClick(PromptDialog dialog) {
                            dialog.dismiss();
                        }
                    }).show();

        }
    }


    @OnClick(R.id.link_forgotpass)
    public void onViewForgotPasswordClicked() {
        Intent start = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
        startActivity(start);
        finish();

    }

    @Override
    protected void loadMethod() {
        insertLogin(inputEmail.getText().toString(), inputPassword
                .getText().toString());
    }
}
