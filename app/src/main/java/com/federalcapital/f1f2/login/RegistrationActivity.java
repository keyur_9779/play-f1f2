package com.federalcapital.f1f2.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.federalcapital.f1f2.BaseActivity;
import com.federalcapital.f1f2.MainActivity;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.federalcapital.f1f2.utils.Constants;
import com.federalcapital.f1f2.utils.PasswordEditText;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import org.json.JSONObject;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity {

    private static final String TAG = RegistrationActivity.class.getSimpleName();
    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_mobile)
    EditText mobile;
    @BindView(R.id.input_password)
    PasswordEditText inputPassword;
    @BindView(R.id.confirm_password)
    PasswordEditText confirmPassword;
    @BindView(R.id.btn_signup)
    AppCompatButton btnSignup;
    @BindView(R.id.link_login)
    TextView linkLogin;

    @BindView(R.id.checkbox_terms)
    AppCompatCheckBox checkboxTerms;
    @BindView(R.id.txt_signup)
    TextView txtSignup;
    @BindView(R.id.signup_scroll)
    ScrollView signupScroll;
    @BindView(R.id.txt_terms)
    TextView txtTerms;
    @BindView(R.id.input_organization)
    EditText input_organization;
    @BindView(R.id.input_city)
    EditText input_city;
    @BindView(R.id.input_stream)
    EditText input_stream;
    @BindView(R.id.input_referral)
    EditText input_referral;


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {

            inputEmail.setText(getIntent().getExtras().getString(Constants.email, Constants.EMPTY));

            inputName.setText(getIntent().getExtras().getString(Constants.name, Constants.EMPTY));
        }

    }

    @OnClick(R.id.btn_signup)
    public void onViewClicked() {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        String confirmPasswordUser = confirmPassword.getText().toString();
        String name = inputName.getText().toString();
        String phone = mobile.getText().toString();
        String referral = input_referral.getText().toString();
        String organization = input_organization.getText().toString();
        String city = input_city.getText().toString();
        String stream = input_stream.getText().toString();

        if (checkboxTerms.isChecked()) {
            String termsAccept = "yes";


            if (!name.isEmpty()) {
                if (!email.isEmpty() && isValidEmail(email)) {
                    if (!password.isEmpty() && password.length() > 5) {
                        if (password.equals(confirmPasswordUser)) {
                            if (!phone.isEmpty() && phone.length() == 10) {

                                if (!organization.isEmpty()) {
                                    if (!city.isEmpty()) {

                                        if (!stream.isEmpty()) {
                                            if (haveNetworkConnection()) {
                                                register(email, password, confirmPasswordUser, name, phone, termsAccept, referral, organization, city, stream);
                                            } else {
                                                showNetworkError();
                                            }

                                        } else {
                                            input_stream.setError("Please enter your stream.");
                                            input_stream.requestFocus();
                                        }

                                    } else {
                                        input_city.setError("Please enter your city.");
                                        input_city.requestFocus();
                                    }

                                } else {
                                    input_organization.setError("Please enter your organization name.");
                                    input_organization
                                            .requestFocus();
                                }

                            } else {
                                mobile.setError("Please enter valid mobile number");
                                mobile.requestFocus();

                            }

                        } else {
                            confirmPassword.setError("Please re-enter password");
                            confirmPassword.requestFocus();

                        }

                    } else {
                        inputPassword.setError("Password must be at least 6 characters.");
                        inputPassword.requestFocus();

                    }

                } else {
                    inputEmail.setError("Please enter valid email");
                    inputEmail.requestFocus();
                }

            } else {
                inputName.setError("Please enter name");
                inputName.requestFocus();
            }
        } else {
            Toast.makeText(RegistrationActivity.this, "Please accept terms and condition", Toast.LENGTH_LONG).show();

        }


    }

    @OnClick(R.id.link_login)
    public void onLoginViewClicked() {

        Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    public void register(String email, String password, String confirmPasswordUser, String name, String phone, String termsAccept, String referral, String organization,
                         String city, String stream) {
        hideKeyboard();
        LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this).autoDismiss(false)
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
                .asLoading()
                .show();

        Call<ResponseBody> call;
        call = RetroConfig.retrofit().register(email, password, confirmPasswordUser, name, phone, termsAccept, organization, stream, city, referral);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {


                    loadingPopup.dismiss();

                    if (response.isSuccessful()) {


                        String responseString = response.body().string().trim();

                        JSONObject jsonObject = new JSONObject(responseString);

                        JSONObject jsonObjectInner = jsonObject.getJSONObject("success");
                        String token = jsonObjectInner.getString("token");

                        AppPrefConfig.getPref().setToken(token);
                        AppPrefConfig.getPref().setEmail(email);

                        if (jsonObjectInner.has("name")) {
                            AppPrefConfig.getPref().setUserName(jsonObjectInner.getString("name"));
                        }
                            Intent start = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(start);
                            finish();
                       /* new PromptDialog(RegistrationActivity.this)
                                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                                .setAnimationEnable(true)
                                .setTitleText("Successful")
                                .setContentText("Congratulations!, Your account will be activated in 4 hours")
                                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                                    @Override
                                    public void onClick(PromptDialog dialog) {
                                        dialog.dismiss();
                                        Intent start = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(start);
                                        finish();
                                    }
                                }).show();

                        //}*/


                    } else {
                        Log.e(TAG, "response" + response);
                        if (response.code() == 401) {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            JSONObject jsonObjectInner = jsonObject.getJSONObject("error");

                            Iterator<String> iterator = jsonObjectInner.keys();


                            StringBuffer stringBuffer = new StringBuffer();

                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                String token = jsonObjectInner.getJSONArray(key).getString(0);

                                stringBuffer.append("\n" + token);
                                Log.e(TAG, "response1111" + token);
                            }

                            Toast.makeText(RegistrationActivity.this, stringBuffer.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                loadingPopup.dismiss();
                Toast.makeText(RegistrationActivity.this, "error!! please try again.", Toast.LENGTH_LONG).show();

                Log.e(TAG, "error");
            }
        });


    }

    @OnClick(R.id.txt_terms)
    public void onCheckboxTermsClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://f1f2.in/termsandconditionsf1f2.pdf"));
        startActivity(browserIntent);
    }


    @Override
    protected void loadMethod() {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        String confirmPasswordUser = confirmPassword.getText().toString();
        String name = inputName.getText().toString();
        String phone = mobile.getText().toString();
        String termsAccept = "yes";
        String referral = input_referral.getText().toString();
        String organization = input_organization.getText().toString();
        String city = input_city.getText().toString();
        String stream = input_stream.getText().toString();
        register(email, password, confirmPasswordUser, name, phone, termsAccept, referral, organization, city, stream);
    }
}
