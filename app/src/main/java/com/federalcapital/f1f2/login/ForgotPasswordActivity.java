package com.federalcapital.f1f2.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;

import com.federalcapital.f1f2.BaseActivity;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.refactor.lib.colordialog.PromptDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {

    private static final String TAG = ForgotPasswordActivity.class.getSimpleName();
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.btn_sendemail)
    AppCompatButton btnSendemail;
/*    private String email;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String token;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.btn_sendemail)
    public void onViewClicked() {
        String email = inputEmail.getText().toString();
        if (!email.isEmpty()) {
            if (haveNetworkConnection()) {
                forgotPassword(email);
            } else {
                showNetworkError();
            }


        } else {
            Toast.makeText(ForgotPasswordActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
        }
    }

    public void forgotPassword(String email) {


        hideKeyboard();
        LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(this).autoDismiss(false)
                .dismissOnTouchOutside(false)
                .dismissOnBackPressed(false)
                .asLoading()
                .show();
        Call<ResponseBody> call = RetroConfig.retrofit().forgotPassword(email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                loadingPopup.dismiss();
                try {
                    if (response.isSuccessful()) {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        new PromptDialog(ForgotPasswordActivity.this)
                                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                                .setAnimationEnable(true)
                                .setTitleText("Successful")
                                .setContentText(jsonObject.getString("message"))
                                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                                    @Override
                                    public void onClick(PromptDialog dialog) {
                                        dialog.dismiss();

                                        Intent start = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(start);
                                        finish();
                                    }
                                }).show();
                        /*String responseString = response.body().string().trim();
                        Log.e(TAG, "response" + responseString);
                        JSONObject jsonObject = new JSONObject(responseString);
                        JSONObject jsonObjectInner = new JSONObject(jsonObject.getString("success"));
                        token = jsonObjectInner.getString("token");
                        Log.e("Response", token);
                        sharedPreferences = getSharedPreferences("userToken", MODE_PRIVATE);
                        editor = sharedPreferences.edit();
                        editor.putString(Constants.email, email);
                        editor.putString("token", token);
                        editor.commit();


                        Intent start = new Intent(getApplicationContext(), FeedbackActivity.class);
                        startActivity(start);*/

                    } else {

                        JSONObject jsonObject = new JSONObject(response.errorBody().string());

                        new PromptDialog(ForgotPasswordActivity.this)
                                .setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                                .setAnimationEnable(true)
                                .setTitleText("Failed")
                                .setContentText(jsonObject.getString("message"))
                                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                                    @Override
                                    public void onClick(PromptDialog dialog) {
                                        dialog.dismiss();


                                    }
                                }).show();


                    }
                } catch (Exception e) {
                    Toast.makeText(ForgotPasswordActivity.this, "error!! please try again.", Toast.LENGTH_LONG).show();

                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingPopup.dismiss();
                Toast.makeText(ForgotPasswordActivity.this, "error!! please try again.", Toast.LENGTH_LONG).show();

                Log.e(TAG, "hello not submit");
            }
        });


    }


    @Override
    protected void loadMethod() {
        String email = inputEmail.getText().toString();

        forgotPassword(email);
    }

    @Override
    public void onBackPressed() {
        Intent start = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(start);
        super.onBackPressed();

    }
}
