package com.federalcapital.f1f2;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.federalcapital.f1f2.adapter.RewardPointHistory;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.ViewUtil;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.lxj.xpopup.widget.VerticalRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PointHistoryPopUp extends BottomPopupView {


    @BindView(R.id.recyclerView)
    public VerticalRecyclerView recyclerView;

    @BindView(R.id.error_text)
    public AppCompatTextView error_text;

    @BindView(R.id.progressBar_cyclic)
    public ProgressBar progressBar_cyclic;


    private RewardPointHistory pointHistoryAdapter;


    private Call<ResponseBody> call;
    private Unbinder binder;

    public PointHistoryPopUp(@NonNull Context context) {
        super(context);

    }

    @Override
    protected int getGestureHeight() {
        return ViewUtil.getViewUtil().getGestureNavigation();
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.custom_bottom_popup;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        binder = ButterKnife.bind(this, getPopupImplView());


        recyclerView.setHasFixedSize(true);
        pointHistoryAdapter = new RewardPointHistory();
        recyclerView.setAdapter(pointHistoryAdapter);



        /*header_image.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {

                int pixel = Util.dp2px(16);

                outline.setRoundRect(0, 0, view.getWidth(), (view.getHeight() + pixel), pixel);
            }
        });

        header_image.setClipToOutline(true);*/

        onRefreshData();

    }


    @Override
    public void dismiss() {
        super.dismiss();

        if (pointHistoryAdapter != null) {
            call.cancel();

            pointHistoryAdapter.onDestroy();
            recyclerView.setAdapter(null);
            pointHistoryAdapter = null;

            binder.unbind();

        }
    }


    public void onRefreshData() {

        progressBar_cyclic.setVisibility(VISIBLE);
        call = RetroConfig.retrofit().getoffers();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                progressBar_cyclic.setVisibility(GONE);


                try {

                    if (response.isSuccessful()) {

                        String data = response.body().string();


                        List<String> offers = new ArrayList<>();

                        JSONArray array = new JSONObject(data).getJSONArray("offer");

                        for (int i = 0; i < array.length(); i++) {

                            offers.add(array.getJSONObject(i).getString("url"));
                        }


                        if (!offers.isEmpty()) {

                            if (error_text.getVisibility() == View.VISIBLE) {
                                error_text.setVisibility(GONE);

                            }


                            pointHistoryAdapter.updateGameData(offers);

                        } else {
                            recyclerView.setAdapter(null);
                            error_text.setVisibility(VISIBLE);
                        }


                    }
                } catch (Exception e) {
                    error_text.setVisibility(VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if(progressBar_cyclic == null){
                    return;
                }
                progressBar_cyclic.setVisibility(GONE);
                error_text.setVisibility(VISIBLE);

            }
        });


    }


    @Override
    protected int getMaxHeight() {

        int height = XPopupUtils.getWindowHeight();
        if (ViewUtil.getViewUtil().isFullScreen()) {
            return (height + ViewUtil.getViewUtil().getNavigationBarHeight());
        }
        return height;
    }
}