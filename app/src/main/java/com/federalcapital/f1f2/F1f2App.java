package com.federalcapital.f1f2;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;

public class F1f2App extends Application {

    private static F1f2App mAppComponent;
    public Context appContext;

    public static F1f2App getAppComponent() {
        return mAppComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mAppComponent = this;
        appContext = getApplicationContext();


        /*String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        AppModule.getAppModule().getUserDetails().setDeviceID(android_id);*/

        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);*/
    }
}
