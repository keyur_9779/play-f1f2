package com.federalcapital.f1f2;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.federalcapital.f1f2.adapter.PortfolioAdapter;
import com.federalcapital.f1f2.model.PortfolioModel;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.federalcapital.f1f2.utils.ViewUtil;
import com.google.gson.Gson;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.lxj.xpopup.widget.VerticalRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PortfolioHistoryPopUp extends BottomPopupView implements OnPriceUpadate {


    @BindView(R.id.recyclerView)
    public VerticalRecyclerView recyclerView;

    @BindView(R.id.error_text)
    public AppCompatTextView error_text;
    @BindView(R.id.header_image)
    public AppCompatTextView header_image;

    @BindView(R.id.progressBar_cyclic)
    public ProgressBar progressBar_cyclic;


    private PortfolioAdapter pointHistoryAdapter;


    private Call<ResponseBody> call;
    private Unbinder binder;

    public PortfolioHistoryPopUp(@NonNull Context context) {
        super(context);

    }

    @Override
    protected int getGestureHeight() {
        return ViewUtil.getViewUtil().getGestureNavigation();
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.custom_bottom_popup;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        binder = ButterKnife.bind(this, getPopupImplView());
        header_image.setText(
                "Portfolio"
        );

        recyclerView.setHasFixedSize(true);
        pointHistoryAdapter = new PortfolioAdapter(this);
        recyclerView.setAdapter(pointHistoryAdapter);


        onRefreshData();

    }


    @Override
    public void dismiss() {
        super.dismiss();


        if (pointHistoryAdapter != null) {
            call.cancel();

            pointHistoryAdapter.onDestroy();
            recyclerView.setAdapter(null);
            pointHistoryAdapter = null;

            binder.unbind();
        }


    }


    public void onRefreshData() {

        progressBar_cyclic.setVisibility(VISIBLE);
        call = RetroConfig.retrofit().portfolio("Bearer " + AppPrefConfig.getPref().getToken(),
                "application/x-www-form-urlencoded"
                , "application/json");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                progressBar_cyclic.setVisibility(GONE);


                try {

                    if (response.isSuccessful()) {

                        String data = response.body().string();


                        Gson gson = new Gson();

                        PortfolioModel portfolioModel = gson.fromJson(data, PortfolioModel.class);


                        if (!portfolioModel.getData().isEmpty()) {

                            if (error_text.getVisibility() == View.VISIBLE) {
                                error_text.setVisibility(GONE);

                            }


                            pointHistoryAdapter.updateGameData(portfolioModel.getData());

                        } else {
                            recyclerView.setAdapter(null);
                            error_text.setVisibility(VISIBLE);
                        }


                    }
                } catch (Exception e) {
                    error_text.setVisibility(VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {



            }
        });


    }


    @Override
    protected int getMaxHeight() {

        int height = XPopupUtils.getWindowHeight();
        if (ViewUtil.getViewUtil().isFullScreen()) {
            return (height + ViewUtil.getViewUtil().getNavigationBarHeight());
        }
        return height;
    }

    @Override
    public void updatePrice() {
        onRefreshData();
    }
}