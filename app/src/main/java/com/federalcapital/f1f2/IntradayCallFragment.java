package com.federalcapital.f1f2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.adapter.MyAdapter;
import com.federalcapital.f1f2.model.F1F2Datas;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IntradayCallFragment extends Fragment {


    private static final String TAG = "tedt";
    RecyclerView recyclerView;
    ArrayList<F1F2Datas> ac = new ArrayList<>();
    ProgressBar dotProgressBar;
    private MyAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_positioncall_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dotProgressBar = view.findViewById(R.id.progressBar_cyclic);
        recyclerView = view.findViewById(R.id.recyclerView);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new MyAdapter(getActivity());
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        layout.setSmoothScrollbarEnabled(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(mAdapter);
        // initData();
        readPostion();
    }


    public void searchFileter(String query) {
        mAdapter.filter(query);

    }

    public void readPostion() {
        dotProgressBar.setVisibility(View.VISIBLE);

        Call<ResponseBody> call = RetroConfig.retrofit().readIntraday(AppPrefConfig.getPref().getToken());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject res = null;
                        String responseString = response.body().string().trim();
                        Log.e(TAG, "response fragment" + responseString);
                        try {
                            res = new JSONObject(responseString);
                            String rr = res.getString("data");
                            JSONArray jsonArray = new JSONArray(rr);

                            String rrs = res.getString("data_scr");
                            JSONArray js = new JSONArray(rrs);

                            JSONObject jo = js.getJSONObject(0);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject mJsonObject = jsonArray.getJSONObject(i);
                                Iterator<String> iter = mJsonObject.keys();
                                while (iter.hasNext()) {
                                    String key = iter.next();

                                    try {
                                        String ress = mJsonObject.get(key).toString();
                                        String re_scr = "";
                                        re_scr = jo.get(key).toString();
                                        JSONObject js_ress = new JSONObject(ress);
                                        JSONObject js_scr = new JSONObject(re_scr);
                                        String signal = js_ress.getString("signal");
                                        String scrip = js_scr.getString("stock") + "/-";
                                        String stllow = js_ress.getString("stllow") + "/-";
                                        String stlhigh = js_ress.getString("stlhigh") + "/-";
                                        String rate = js_scr.getString("rate") + "%";

                                        String ex = "", targetHit = "";
                                        try {
                                            ex = js_ress.getString("exit_stl") + "/-";
                                        } catch (JSONException e) {

                                        }
                                        try {
                                            targetHit = js_ress.getString("exit_trg") + "/-";
                                        } catch (JSONException e) {

                                        }
                                        ac.add(new F1F2Datas(key, scrip, signal, stllow, stlhigh, ex, rate, targetHit));

                                    } catch (JSONException e) {
                                        // e.printStackTrace();
                                    }

                                }


                            }
                            dotProgressBar.setVisibility(View.GONE);
                            mAdapter.updateAll(ac);
                            recyclerView.setAdapter(mAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity().getApplicationContext(), " Something went wrong...plz try later", Toast.LENGTH_LONG).show();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong...plz try later", Toast.LENGTH_LONG).show();

                Log.e(TAG, "error");
            }
        });


    }

}