package com.federalcapital.f1f2.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.federalcapital.f1f2.F1f2App;


/**
 * Global class used to save application state into shared pref;
 * object shoud be singleton class created from application class
 *
 * @author keyur thumar
 */
public class AppPrefConfig {

    private static final String PREF_FILE_NAME = "android_rate_pref_file";
    public static AppPrefConfig appConfig;
    private final String PREF_KEY_INSTALL_DATE = "android_rate_install_date";
    private final String PREF_KEY_LAUNCH_TIMES = "android_rate_launch_times";
    private final String PREF_KEY_IS_AGREE_SHOW_DIALOG = "android_rate_is_agree_show_dialog";
    private final String PREF_KEY_REMIND_INTERVAL = "android_rate_remind_interval";
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;

    /**
     * AppConfig is used for initialising shared preferences object for entire application.
     * it will store state of application like registration,device tokenId, phone number etc.
     */
    public AppPrefConfig() {
        String MYPREFS = "f1f2_k_pref";

        settings = F1f2App.getAppComponent().appContext.getSharedPreferences(MYPREFS, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    public static AppPrefConfig getPref() {
        if (appConfig == null) {
            appConfig = new AppPrefConfig();
        }
        return appConfig;
    }


    public String getUserName() {
        return settings.getString(Constants.name, Constants.EMPTY);
    }

    public void setUserName(String s) {
        editor.putString(Constants.name, s);
        editor.apply();
    }

    public String getToken() {
        return settings.getString(Constants.token, Constants.EMPTY);
    }

    public void setToken(String s) {
        editor.putString(Constants.token, s);
        editor.apply();
    }

    public String getEmail() {
        return settings.getString(Constants.email, Constants.EMPTY);
    }

    public void setEmail(String s) {
        editor.putString(Constants.email, s);
        editor.apply();
    }

    public void logOut() {
        editor.clear();
        editor.apply();
    }


}
