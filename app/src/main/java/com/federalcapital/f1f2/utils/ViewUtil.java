package com.federalcapital.f1f2.utils;

import android.content.res.Resources;

import com.federalcapital.f1f2.F1f2App;

public class ViewUtil {

    public static ViewUtil viewUtil;
    private final int height;
    private final int width;
    private final int toolBarHeight = 48;
    private int getNavigationBarHeight;
    private int statusBarHeight;
    private int usedheaderWidth;
    private int childeWidth;
    private int childHeight;
    private boolean isFullScreen = false;
    private int gestureNavigation;

    private ViewUtil() {

        height = Resources.getSystem().getDisplayMetrics().heightPixels;
        width = Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static ViewUtil getViewUtil() {

        if (viewUtil == null) {
            viewUtil = new ViewUtil();
        }
        return viewUtil;
    }

    public int getStatusHeight() {
        return toolBarHeight;
    }

    public int getUsedheaderWidth() {
        return usedheaderWidth;
    }

    public int getHeight() {

        // //Log.d(Constant.TAG, "height : " + height);
        return height;
    }

    public int getWidth() {

        ////Log.d(Constant.TAG, "width : " + width);
        return width;
    }

    public int getNavigationBarHeight() {

        if (getNavigationBarHeight > 0) {
            return getNavigationBarHeight;
        }
        Resources resources = F1f2App.getAppComponent().getResources();

        int id = resources.getIdentifier("navigation_bar_height",
                "dimen", "android");


        if (id > 0) {
            getNavigationBarHeight = resources.getDimensionPixelSize(id);
        } else {
            getNavigationBarHeight = 0;
        }


        ////Log.d(Constant.TAG, "NavigationBar Heightr : " + getNavigationBarHeight);


        return getNavigationBarHeight;
    }

    public int getStatusBarHeight() {

        if (statusBarHeight > 0) {
            return statusBarHeight;
        }

        Resources resources = F1f2App.getAppComponent().getResources();


        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = resources.getDimensionPixelSize(resourceId);
        } else {
            statusBarHeight = 0;
        }
        return statusBarHeight;
    }

    public void setHeaderUsedWidth(int prevChildBottom) {

        usedheaderWidth = prevChildBottom;
    }

    public void setChildWidth(int tempWidth) {

        childeWidth = tempWidth;
    }

    public int getChildHeight() {
        return childHeight;
    }

    public void setChildHeight(int ch) {
        this.childHeight = ch;
    }

    public int getToolBarHeight() {
        return toolBarHeight;
    }

    public int getGetNavigationBarHeight() {
        return getNavigationBarHeight;
    }

    public int getChildeWidth() {
        return childeWidth;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean b) {

        isFullScreen = b;
    }

    public int getGestureNavigation() {
        return gestureNavigation;
    }

    public void setGestureNavigation(int value) {
        gestureNavigation = value;
    }
}
