package com.federalcapital.f1f2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.OnPriceUpadate;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.model.PortfolioDataModel;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PortfolioAdapter extends HomeAdapter {


    private List<PortfolioDataModel> gameLevels = new ArrayList<>();

    private OnPriceUpadate onPriceUpadate;

    public PortfolioAdapter(OnPriceUpadate callback) {

        onPriceUpadate = callback;

    }


    @Override
    public void loadAd() {

    }

    @Override
    public void updateGameData(Object offers) {

        gameLevels.clear();
        gameLevels.addAll((Collection<? extends PortfolioDataModel>) offers);
        notifyDataSetChanged();
        ((Collection<? extends PortfolioDataModel>) offers).clear();
    }

    /*public void updateGameData(List<GameOffer> levels) {
        gameLevels.clear();

        gameLevels = levels;
        notifyDataSetChanged();
    }*/


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        return new PortfolioViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.portfolio_item,
                parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);


        PortfolioViewHolder pointViewHolder = (PortfolioViewHolder) viewHolder;
        PortfolioDataModel portfolioDataModel = gameLevels.get(position);

        pointViewHolder.txt_qty_value.setText(portfolioDataModel.getSharebalance());
        pointViewHolder.txt_company.setText(portfolioDataModel.getCompanyname());
        pointViewHolder.txt_invested_amount.setText(portfolioDataModel.getTotal());
        pointViewHolder.txt_gross_pl.setText(portfolioDataModel.getCmp());
        pointViewHolder.txt_buy_avg_value.setText(portfolioDataModel.getBuyavg());

        String profitLoss = portfolioDataModel.getProfitloss();
        if (profitLoss.startsWith("-")) {

            pointViewHolder.txt_pl.setTextColor(Color.RED);
        } else if (profitLoss.startsWith("0")) {
            pointViewHolder.txt_pl.setTextColor(Color.BLACK);
        } else {
            pointViewHolder.txt_pl.setTextColor(Color.GREEN);
        }

        pointViewHolder.txt_pl.setText(portfolioDataModel.getProfitloss());

    }


    @Override
    public int getItemCount() {

        return gameLevels.size();

    }


    @Override
    public void onDestroy() {
        gameLevels.clear();
    }


    public class PortfolioViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_qty_value)
        TextView txt_qty_value;

        @BindView(R.id.txt_company)
        TextView txt_company;

        @BindView(R.id.txt_invested_amount)
        TextView txt_invested_amount;

        @BindView(R.id.txt_gross_pl)
        TextView txt_gross_pl;
        @BindView(R.id.txt_buy_avg_value)
        TextView txt_buy_avg_value;


        @BindView(R.id.txt_pl)
        TextView txt_pl;


        public PortfolioViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


        @OnClick(R.id.cell_view)
        public void onCellClicked(View view) {


            PortfolioDataModel value = gameLevels.get(getAdapterPosition());

            Context context = view.getContext();

            new XPopup.Builder(context)

                    .dismissOnBackPressed(true)
                    .autoOpenSoftInput(true)
                    .isRequestFocus(false)
                    .dismissOnTouchOutside(true)
                    .asInputConfirm(value.getCompanyname()
                            , "<font color=#000000>Price : </font>" + value.getCmp(), "", "Quantity",
                            new OnInputConfirmListener() {
                                @Override
                                public void onConfirm(String text) {

// sell stock price
                                    LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(context).autoDismiss(false)
                                            .dismissOnTouchOutside(false)
                                            .dismissOnBackPressed(false)
                                            .asLoading()
                                            .show();

                                    Call<ResponseBody> call = RetroConfig.retrofit().purchaseShare("Bearer " + AppPrefConfig.getPref().getToken()
                                            /*, "application/x-www-form-urlencoded"
                                            , "application/json"*/, value.getCompanyname()
                                            ,

                                            value.getCmp().replace("/-", ""), text, "SELL");
                                    call.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            try {

                                                loadingPopup.dismiss();

                                                if (response.isSuccessful()) {
                                                    String responseString = response.body().string().trim();
                                                    JSONObject jsonObject = new JSONObject(responseString
                                                    );
                                                    if (jsonObject.has("alert")) {


                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert"), Toast.LENGTH_LONG
                                                        ).show();

                                                    } else if (jsonObject.has("alert1")) {


                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert1"), Toast.LENGTH_LONG
                                                        ).show();

                                                    } else {
                                                        onPriceUpadate.updatePrice();
                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_LONG
                                                        ).show();
                                                    }

                                                } else {

                                                    Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();

                                                }
                                            } catch (Exception e) {
                                                Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();


                                            }


                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                                            loadingPopup.dismiss();
                                            Toast.makeText(context.getApplicationContext(),
                                                    "Please try again!!", Toast.LENGTH_LONG).show();

                                        }
                                    });
                                }


                            }, new

                                    OnCancelListener() {
                                        @Override
                                        public void onCancel(String text) {
// bu
                                            LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(context).autoDismiss(false)
                                                    .dismissOnTouchOutside(false)
                                                    .dismissOnBackPressed(false)
                                                    .asLoading()
                                                    .show();

                                            Call<ResponseBody> call = RetroConfig.retrofit().purchaseShare(
                                                    "Bearer " + AppPrefConfig.getPref().getToken()
                                                    /*"application/x-www-form-urlencoded"
                                                    , "application/json"*/, value.getCompanyname(),

                                                    value.getCmp()

                                                            .replace("/-", ""), text, "BUY");
                                            call.enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                    try {

                                                        loadingPopup.dismiss();

                                                        if (response.isSuccessful()) {
                                                            String responseString = response.body().string().trim();


                                                            JSONObject jsonObject = new JSONObject(responseString
                                                            );
                                                            //{"alert":"Stock Market is Closed"}
                                                            if (jsonObject.has("alert")) {


                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert"), Toast.LENGTH_LONG
                                                                ).show();

                                                            } else if (jsonObject.has("alert1")) {


                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert1"), Toast.LENGTH_LONG
                                                                ).show();

                                                            } else {

                                                                onPriceUpadate.updatePrice();
                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_LONG
                                                                ).show();
                                                            }


                                                            Log.d(
                                                                    "keyur", responseString

                                                            );


                                                        } else {

                                                            Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();

                                                        }
                                                    } catch (Exception e) {
                                                        Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();


                                                    }


                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                    loadingPopup.dismiss();
                                                    Toast.makeText(context.getApplicationContext(),
                                                            "Please try again!!", Toast.LENGTH_LONG).show();

                                                }
                                            });

                                        }
                                    })

                    .

                            show();

        }
    }


}