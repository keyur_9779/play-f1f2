package com.federalcapital.f1f2.adapter;

public interface OnNavigationItemClick {

    void onNavigationItemSelected(int position);
}
