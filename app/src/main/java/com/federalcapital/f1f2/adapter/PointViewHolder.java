package com.federalcapital.f1f2.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PointViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.navigation_image)
    public AppCompatImageView header_text;


    public PointViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

}
