package com.federalcapital.f1f2.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.federalcapital.f1f2.OnPriceUpadate;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.model.F1F2Datas;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAdapter extends BaseAdpater<F1F2Datas, MyAdapter.MyHolder> {


    private OnPriceUpadate onPriceUpadate;

    public MyAdapter(Activity context) {
        super(context);
        onPriceUpadate = (OnPriceUpadate) context;
    }

    @Override
    protected MyHolder getViewHolder(View itemView, int viewType) {
        return new MyHolder(itemView);
    }

    @Override
    protected void convert(MyHolder holder, int position, F1F2Datas f1F2Datas) {

        String companyName = f1F2Datas.getCompanyName();
        String price = f1F2Datas.getStockPrice();
        String rate = f1F2Datas.getRate();
        String target = f1F2Datas.getTarget1();
        String stopLess = f1F2Datas.getStoploss();
        String exit = f1F2Datas.getExit();
        String signal = f1F2Datas.getSignal();
        String exitTar = f1F2Datas.getExitTar();

        holder.txtComp.setText(companyName);
        holder.txtPrice.setText(price);
        holder.txtRate.setText(rate);
        holder.txtTarget1.setText(target);
        holder.txtStoploss.setText(stopLess);
        holder.button.setText(f1F2Datas.getSignal());
        holder.button.setTextColor(Color.parseColor("#FF0000"));

        if (exit != "") {
            holder.button.setText("EXIT");
            holder.button.setTextColor(Color.parseColor("#FF0000"));
        } else if (exitTar != "") {
            holder.button.setText("TARGET HIT");
            holder.button.setTextColor(Color.parseColor("#FF0000"));
        } else {
            if (signal.equals("BUY")) {
                holder.button.setText(signal);
                holder.button.setTextColor(Color.parseColor("#006400"));

            } else if (signal.equals("SELL")) {
                holder.button.setText(signal);
                holder.button.setTextColor(Color.parseColor("#000080"));
            } else {
                holder.button.setText("HOLD");
                holder.button.setTextColor(Color.parseColor("#FFA500"));

            }
        }
        // signal =  holder.button.getText().toString();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    @Override
    protected int getLayoutId(int viewType) {
        if (viewType == 0) {
            return R.layout.position_call_list_items;
        } else {
            return R.layout.position_call_list_items;
        }
    }

    public void filter(String query) {

        data.clear();

        if (query.isEmpty()) {

            data.addAll(keepData);
        } else {


            for (F1F2Datas ad : keepData) {

                if (ad.getCompanyName().toLowerCase().contains(query.toLowerCase()) || ad.getCompanyName().toLowerCase().contains(query.toLowerCase())) {
                    data.add(ad);
                }

            }
        }
        notifyDataSetChanged();


    }

    class MyHolder extends BaseViewHolder {

        @BindView(R.id.txt_comp)
        TextView txtComp;
        @BindView(R.id.textView3)
        TextView textView3;
        @BindView(R.id.txt_price)
        TextView txtPrice;
        @BindView(R.id.price)
        RelativeLayout price;
        @BindView(R.id.txt_rate)
        TextView txtRate;
        @BindView(R.id.rate)
        RelativeLayout rate;
        @BindView(R.id.txt_target1)
        TextView txtTarget1;

        @BindView(R.id.txt_stoploss)
        TextView txtStoploss;

        @BindView(R.id.button)
        TextView button;


        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.cell_view)
        public void onCellClicked(View view) {


            F1F2Datas value = data.get(getAdapterPosition());

            Context context = view.getContext();

            new XPopup.Builder(context)
                    .dismissOnBackPressed(true)
                    .autoOpenSoftInput(true)
                    .isRequestFocus(false)
                    .dismissOnTouchOutside(true)
                    .asInputConfirm(value.getCompanyName(), "<font color=#000000>Price : </font>" + value.getStockPrice(), "", "Quantity",
                            new OnInputConfirmListener() {
                                @Override
                                public void onConfirm(String text) {


// sell stock price

                                    LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(context).autoDismiss(false)
                                            .dismissOnTouchOutside(false)
                                            .dismissOnBackPressed(false)
                                            .asLoading()
                                            .show();

                                    Call<ResponseBody> call = RetroConfig.retrofit().purchaseShare("Bearer " + AppPrefConfig.getPref().getToken()
                                            /*, "application/x-www-form-urlencoded"
                                            , "application/json"*/, value.getCompanyName(),

                                            value.getStockPrice().replace("/-", ""), text, "SELL");
                                    call.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            try {

                                                loadingPopup.dismiss();

                                                if (response.isSuccessful()) {
                                                    String responseString = response.body().string().trim();
                                                    JSONObject jsonObject = new JSONObject(responseString
                                                    );
                                                    if (jsonObject.has("alert")) {


                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert"), Toast.LENGTH_LONG
                                                        ).show();

                                                    } else if (jsonObject.has("alert1")) {


                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert1"), Toast.LENGTH_LONG
                                                        ).show();

                                                    } else {

                                                        onPriceUpadate.updatePrice();

                                                        Toast.makeText(context.getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_LONG
                                                        ).show();
                                                    }

                                                } else {

                                                    Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();

                                                }
                                            } catch (Exception e) {
                                                Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                                            loadingPopup.dismiss();
                                            Toast.makeText(context.getApplicationContext(),
                                                    "Please try again!!", Toast.LENGTH_LONG).show();

                                        }
                                    });
                                }


                            }, new

                                    OnCancelListener() {
                                        @Override
                                        public void onCancel(String text) {
// bu
                                            LoadingPopupView loadingPopup = (LoadingPopupView) new XPopup.Builder(context).autoDismiss(false)
                                                    .dismissOnTouchOutside(false)
                                                    .dismissOnBackPressed(false)
                                                    .asLoading()
                                                    .show();

                                            Call<ResponseBody> call = RetroConfig.retrofit().purchaseShare(
                                                    "Bearer " + AppPrefConfig.getPref().getToken()
                                                    /*"application/x-www-form-urlencoded"
                                                    , "application/json"*/, value.getCompanyName(),

                                                    value.getStockPrice().replace("/-", ""), text, "BUY");
                                            call.enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                    try {

                                                        loadingPopup.dismiss();

                                                        if (response.isSuccessful()) {
                                                            String responseString = response.body().string().trim();


                                                            JSONObject jsonObject = new JSONObject(responseString
                                                            );
                                                            //{"alert":"Stock Market is Closed"}
                                                            if (jsonObject.has("alert")) {


                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert"), Toast.LENGTH_LONG
                                                                ).show();

                                                            } else if (jsonObject.has("alert1")) {


                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("alert1"), Toast.LENGTH_LONG
                                                                ).show();

                                                            } else {

                                                                onPriceUpadate.updatePrice();
                                                                Toast.makeText(context.getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_LONG
                                                                ).show();
                                                            }


                                                        } else {

                                                            Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();

                                                        }
                                                    } catch (Exception e) {
                                                        Toast.makeText(context.getApplicationContext(), "Please try again!!", Toast.LENGTH_LONG).show();


                                                    }


                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                    loadingPopup.dismiss();
                                                    Toast.makeText(context.getApplicationContext(),
                                                            "Please try again!!", Toast.LENGTH_LONG).show();

                                                }
                                            });

                                        }
                                    })

                    .

                            show();

        }
    }


}
