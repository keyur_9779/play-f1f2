package com.federalcapital.f1f2.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class NavigationViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.navigation_image)
    public AppCompatImageView navigation_image;

    @BindView(R.id.navigation_text)
    public AppCompatTextView navigation_text;


    public NavigationViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}

