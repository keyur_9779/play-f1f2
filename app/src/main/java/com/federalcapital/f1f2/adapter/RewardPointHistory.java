package com.federalcapital.f1f2.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.retrofit.GlideApp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RewardPointHistory extends HomeAdapter {


    private List<String> gameLevels = new ArrayList<>();


    public RewardPointHistory() {


    }


    @Override
    public void loadAd() {

    }

    @Override
    public void updateGameData(Object offers) {

        gameLevels.addAll((Collection<? extends String>) offers);
        notifyDataSetChanged();
        ((Collection<? extends String>) offers).clear();
    }

    /*public void updateGameData(List<GameOffer> levels) {
        gameLevels.clear();

        gameLevels = levels;
        notifyDataSetChanged();
    }*/


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        return new PointViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_item,
                parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);


        PointViewHolder pointViewHolder = (PointViewHolder) viewHolder;


        GlideApp.with(pointViewHolder.header_text.getContext()).load(gameLevels.get(position)).into(pointViewHolder.header_text);


    }


    @Override
    public int getItemCount() {

        return gameLevels.size();

    }


    @Override
    public void onDestroy() {
        gameLevels.clear();
    }


}