package com.federalcapital.f1f2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.model.NavigationModel;

import java.util.ArrayList;
import java.util.List;

public class NavigationAdapter extends HomeAdapter implements View.OnClickListener {


    private List<NavigationModel> navigationModel = new ArrayList<>();

    private OnNavigationItemClick callback;

    public NavigationAdapter(OnNavigationItemClick listener) {

        callback = listener;

        //navigationModel.add(new NavigationModel(R.drawable.ic_start_player, "Top 50 Players"));
        //navigationModel.add(new NavigationModel(R.drawable.ic_supervised_user_circle, "My Profile"));
        navigationModel.add(new NavigationModel(R.drawable.ic_portfolio, "Portfolio"));
        navigationModel.add(new NavigationModel(R.drawable.ic_contact_support, "Help"));
        //navigationModel.add(new NavigationModel(R.drawable.ic_settings, "Settings"));
        navigationModel.add(new NavigationModel(R.drawable.ic_menu_send, "Feedback"));
        navigationModel.add(new NavigationModel(R.drawable.ic_action_star, "Rate"));
        navigationModel.add(new NavigationModel(R.drawable.ic_menu_share, "Share"));
        navigationModel.add(new NavigationModel(R.drawable.ic_logout, "Logout"));
        navigationModel.add(new NavigationModel(R.drawable.ic_pp, "Privacy Policy"));

    }

    @Override
    public void updateGameData(Object gameLevel) {

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NavigationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        NavigationViewHolder viewHolder = (NavigationViewHolder) holder;
        NavigationModel model = navigationModel.get(position);
        viewHolder.navigation_image.setImageResource(model.getResourceID());
        viewHolder.navigation_text.setText(model.getName());
        viewHolder.itemView.setTag(position);
        viewHolder.itemView.setOnClickListener(this);

    }

    @Override
    public void onDestroy() {
        callback = null;
        navigationModel.clear();

    }

    @Override
    public void loadAd() {

    }

    @Override
    public int getItemCount() {
        return navigationModel.size();
    }

    @Override
    public void onClick(View view) {
        callback.onNavigationItemSelected((Integer) view.getTag());
    }

}
