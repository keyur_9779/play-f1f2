package com.federalcapital.f1f2.adapter;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by PC大佬 on 2018/2/9.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }
}

