package com.federalcapital.f1f2;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.federalcapital.f1f2.adapter.NavigationAdapter;
import com.federalcapital.f1f2.adapter.OnNavigationItemClick;
import com.federalcapital.f1f2.help.FeedbackActivity;
import com.federalcapital.f1f2.help.HelpActivity;
import com.federalcapital.f1f2.login.LoginActivity;
import com.federalcapital.f1f2.retrofit.RetroConfig;
import com.federalcapital.f1f2.utils.AppPrefConfig;
import com.federalcapital.f1f2.utils.ViewUtil;
import com.google.android.material.tabs.TabLayout;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnNavigationItemClick, OnPriceUpadate {

    TextView navUsername;
    TextView navEmail;
    @BindView(R.id.navigation_recycler_view)
    RecyclerView navigation_recycler_view;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    boolean isSwap = false;
    private MaterialSearchView searchView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter adapter;
    private ImageView menu;
    private Unbinder unbinder;
    private NavigationAdapter navigationAdapter;
    private BasePopupView basepopUP;

    @Override

    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        unbinder = null;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            drawer.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {

                    ViewUtil.getViewUtil().setGestureNavigation(insets.getSystemWindowInsetBottom());
                    return insets;
                }
            });
        }

        isFullScreen();

    }

    private void updateMoney() {
        Call<ResponseBody> call = RetroConfig.retrofit().getMoney("Bearer " + AppPrefConfig.getPref().getToken()
                , "application/x-www-form-urlencoded"
                , "application/json");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {


                    if (response.isSuccessful()) {
                        String responseString = response.body().string().trim();

                        JSONObject jsonObject = new JSONObject(responseString);

                        tabLayout.getTabAt(0).setText("Virtual Money : INR " +

                                String.format("%.2f", jsonObject.getJSONObject("data")
                                        .getDouble("balance"))

                        );


                    }
                } catch (Exception e) {


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }
        });
    }

    public void isFullScreen() {
        try {

            boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

           /* if(!hasMenuKey && !hasBackKey) {
                ViewUtil.getViewUtil().setFullScreen(false);
            }*/
            boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);
            if (hasHomeKey) {
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);


        navUsername = (TextView) findViewById(R.id.txt_header_name);
        navEmail = (TextView) findViewById(R.id.txt_header_email);

        menu = findViewById(R.id.menu);

        ImageButton imageButton = (ImageButton) findViewById(R.id.more);
        ImageButton offerButton = (ImageButton) findViewById(R.id.offer);

        offerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                basepopUP = new XPopup.Builder(v.getContext())
                        .moveUpToKeyboard(false)
                        .dismissOnTouchOutside(false)
                        .autoDismiss(false)
                        .asCustom(new PointHistoryPopUp(v.getContext()))
                        .show();

            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.showSearch();
            }
        });
        sqliteCheck();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.ic_action_action_search);
        //searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Fragment fragmetn = adapter.getItem(viewPager.getCurrentItem());

                if (fragmetn instanceof PositionCallFragment) {

                    ((PositionCallFragment) fragmetn).searchFileter(query);
                } else {
                    ((IntradayCallFragment) fragmetn).searchFileter(query);

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                if (!isSwap) {
                    isSwap = false;
                    Fragment fragmetn = adapter.getItem(viewPager.getCurrentItem());

                    if (fragmetn instanceof PositionCallFragment) {
                        ((PositionCallFragment) fragmetn).searchFileter("");
                    } else {
                        ((IntradayCallFragment) fragmetn).searchFileter("");
                    }
                }
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });


        navigation_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        navigation_recycler_view.setHasFixedSize(true);
        //Use this now

        navigationAdapter = new NavigationAdapter(this);

        navigation_recycler_view.setAdapter(navigationAdapter);
        updateMoney();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PositionCallFragment(), "Virtual Money : INR 200000");
        //adapter.addFragment(new IntradayCallFragment(), "Intraday call");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

                if (searchView.isSearchOpen()) {
                    isSwap = true;
                    searchView.closeSearch();


                    int index = position == 1 ? 0 : 1;

                    Fragment fragmetn = adapter.getItem(index);

                    if (fragmetn instanceof PositionCallFragment) {

                        ((PositionCallFragment) fragmetn).searchFileter("");
                    } else {
                        ((IntradayCallFragment) fragmetn).searchFileter("");

                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    public void onBackPressed() {

        if (basepopUP != null) {
            if (!basepopUP.isDismiss()) {
                basepopUP.dismiss();
                basepopUP = null;
                updateMoney();
                return;
            }
        }

        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    private void sqliteCheck() {
        navEmail.setText(AppPrefConfig.getPref().getEmail());
        navUsername.setText(AppPrefConfig.getPref().getUserName());
    }

    @Override
    public void onNavigationItemSelected(int position) {


        switch (position) {

            case 0:
                basepopUP = new XPopup.Builder(this)
                        .moveUpToKeyboard(true)
                        .dismissOnTouchOutside(false)
                        .autoDismiss(false)
                        .asCustom(new PortfolioHistoryPopUp(
                                this))
                        .show();
                break;

            case 6:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://f1f2.in/termsandconditionsf1f2.pdf"));
                startActivity(browserIntent);
                break;
            case 5:
                AppPrefConfig.getPref().logOut();
                Intent intent1 = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent1);
                finish();
                break;
            case 4:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(shareIntent, "Share your thoughts"));
                break;
            case 1:
                Intent asd = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(asd);
                break;
            case 2:
                Intent intentFeedback = new Intent(MainActivity.this, FeedbackActivity.class);
                startActivity(intentFeedback);
                break;
            case 3:
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());

                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {

                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }

                break;

            default:
        }
        drawer.closeDrawer(Gravity.LEFT);
    }


    @Override
    public void updatePrice() {

        updateMoney();

    }
}
