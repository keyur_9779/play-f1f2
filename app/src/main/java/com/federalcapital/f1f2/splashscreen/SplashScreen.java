package com.federalcapital.f1f2.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.federalcapital.f1f2.BaseActivity;
import com.federalcapital.f1f2.R;
import com.federalcapital.f1f2.login.LoginActivity;

public class SplashScreen extends BaseActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        launchMain();

    }

    private void launchMain() {


        if (haveNetworkConnection()) {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent launchNextActivity;
                    launchNextActivity = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(launchNextActivity);
                    finish();
                }
            }, 1000);


        } else {
            showNetworkError();

        }

    }


    @Override
    protected void loadMethod() {
        launchMain();
    }
}

