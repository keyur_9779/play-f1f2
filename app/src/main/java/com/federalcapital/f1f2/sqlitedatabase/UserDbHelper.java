package com.federalcapital.f1f2.sqlitedatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Clusterbean Technologies Pvt. Ltd. on 25/4/18.
 */
public class UserDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_QUERY_REGISTER =
            "CREATE TABLE " + UserContract.Register.TABLE_NAME + "(" +
                    UserContract.Register.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    UserContract.Register.NAME + " TEXT, " +
                    UserContract.Register.EMAIL + " TEXT, " +
                    UserContract.Register.PASSWORD + " TEXT, " +
                    UserContract.Register.MOBILE + " TEXT, " +
                    UserContract.Register.ACTIVE + " TEXT);";
    private static String DATABASE_NAME = "F1F2.DB";
    private static UserDbHelper mInstance = null;
    Context context;
    private String TAG = UserDbHelper.class.getSimpleName();

    public UserDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_QUERY_REGISTER);
        Log.e(TAG, "Table created ..");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {


    }


    /**
     * ADDING DATA TO DB
     */


    public void addRegisterDetails(SQLiteDatabase db, String name, String email, String password, String mobile, String active) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(UserContract.Register.NAME, name);
        contentValues.put(UserContract.Register.EMAIL, email);
        contentValues.put(UserContract.Register.PASSWORD, password);
        contentValues.put(UserContract.Register.MOBILE, mobile);
        contentValues.put(UserContract.Register.ACTIVE, active);


//Add data


        db.insert(UserContract.Register.TABLE_NAME, null, contentValues);


    }


    /**
     * UPDATE TABLES
     */
    public void updateRegisterDetails(SQLiteDatabase db, String active, String email) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserContract.Register.ACTIVE, active);

        db.update(UserContract.Register.TABLE_NAME, contentValues, UserContract.Register.EMAIL + "=" + "'" + email + "'", null);
    }


    /**
     * Get Data from DB
     */
    public Cursor getRegistrationDetails(SQLiteDatabase db) {
        Cursor cursor;
        String[] projections = {UserContract.Register.NAME, UserContract.Register.EMAIL,
                UserContract.Register.MOBILE, UserContract.Register.PASSWORD,
                UserContract.Register.ACTIVE
        };

        cursor = db.query(UserContract.Register.TABLE_NAME, projections, null, null, null, null, null);
        return cursor;
    }


    /**
     * Delete data from DB
     */

    public void deleteRegisterDetails(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("delete from " + UserContract.Register.TABLE_NAME);
    }


    public String deleterRegisterDetailsById(String id, SQLiteDatabase sqLiteDatabase) {

        String selection = UserContract.Register.ID + " = ? ";
        String[] selection_args = {id};
        sqLiteDatabase.delete(UserContract.Register.TABLE_NAME, selection, selection_args);
        return "success";

    }

}
