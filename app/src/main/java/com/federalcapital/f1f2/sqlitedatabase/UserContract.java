package com.federalcapital.f1f2.sqlitedatabase;


public class UserContract {


    public static abstract class Register {
        public static final String TABLE_NAME = "userdetails";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String MOBILE = "mobile";
        public static final String ACTIVE = "active";

    }
}
